# Hive Overview

Hive is a modular backend for a turn-based multiplayer gaming platform. It's written in Node.js and uses MongoDB for data storage.

The project was terminated after a year of development, just before the release (you can read about it [here](http://gamasutra.com/blogs/IvanMir/20150217/236560/Postmortem_Traps_for_Friends__our_attempt_at_fair_IAP_multiplayer.php)).

So Hive is an almost completed solution but we have no use for it anymore.

It supports shared player accounts and is ready for any turn-based game module. The architecture is also made for high CCU.

It's licensed under Apache 2.0.

## Repositories
Check out the /dev branches to get the working parts:

* https://bitbucket.org/combocats/hive-deploy
* https://bitbucket.org/combocats/hive-common
* https://bitbucket.org/combocats/hive-main-server
* https://bitbucket.org/combocats/hive-game-server
* https://bitbucket.org/combocats/hive-admin-frontend

Early development:

* https://bitbucket.org/combocats/hive-analytics
* https://bitbucket.org/combocats/hive-analytics-collector

Not updated for a long time but probably will work:

* https://bitbucket.org/combocats/hive-demo-frontend/

## Credits

**[Lodoss Team](http://www.lodossteam.com/)**

* [Zahar Andreev](mailto:zahar@lodossteam.com) - backend programming
* [Alex Kotenko](mailto:alex@lodossteam.com) - backend programming

**[ComboCats](http://combocats.com/)**

* [Ivan Mir](https://twitter.com/ivmirx) - CTO
* Yuriy Dumbrovskiy - client programming
* Gleb Bulah - client programming
* Jan Mašarski - client programming

# Hive Deploy

## Setup

Before running any playbook you need to install Ansible => 1.6, the bast way is to
keep ansible repo and start the latest version from there.

 + Clone [Ansible repo](https://github.com/ansible/ansible)
 + in the tab you want to use ansible type `source /path/to/ansible/repo/hacking/env-setup`
 and you will see the latest ansible been installed

You can read more [here](https://github.com/ansible/ansible/tree/devel/hacking).

After that you MUST check that config file `ansible.cfg` has correct path to ssh key.
Open the file and find line `private_key_file=~/.ssh/combocats/id_rsa`, change this line if needed.

## Description

We use [Ansible](http://www.ansibleworks.com/) as main deployment tool.

In this repo you will find different playbooks:

 + `hive-main-server-setup.yml`
 + `hive-game-server-setup.yml`
 + `hive-main-server.yml`
 + `hive-game-server.yml`
 + `hive-balancer.yml`
 + `redis.yml`
 + `mongodb-cluster.yml`
 + `hive-analytics.yml`
 + `hive-analytics-collector.yml`

All ip addresses are in `hosts` file.

For Ansible configuration (remote user, ssh keys etc.) see `ansible.cfg`.

All the playbooks has different tags for more granular execution. In example, if you
need to update only main server part you should type something like:

    ansible-playbook hive-analytics.yml -t main

You can look at all avaliable tags in concreet .yml file. If you need more then one tag just
use comma separation (like `-t main,upstart`).

### hive-main-server-setup.yml and hive-game-server-setup.yml

Initial setup and project deploy. Will install everything on a **clear** machine.

Can be executed with `-t` argument (the tag to proccess).
For example, if you want to check/update git and postfix you should run:

    ansible-playbook hive-main-server-setup.yml -t git,postfix

Main tasks:

 + setup environment (nodejs, monit, git, logrotate, postfix)
 + create start-up script for our project
 + clone and setup project of specified tag-version (from specified branch)

### hive-main-server.yml

Used to deploy `hive-main-server` project. Uses only git.

Main tasks:

 + clone and setup project of specified tag-version (from specified branch)

### hive-game-server.yml

Used to deploy `hive-game-server` project. Uses only git.

Main tasks:

 + clone and setup project of specified tag-version (from specified branch)

### hive-balancer.yml

Used to deploy NGINX as single end-point for all other services.
We need balancer in order to split requests among our game servers and main server.

Main tasks:

 + install nginx
 + create config files

### redis.yml

Used to deploy Redis server.

### mongodb-cluster.yml

Used to deploy mongodb cluster. For now it will be a single mongod process on
a specified server. Later it will user replications. If we'll came across performance issues,
it will be a cluster of shards.

Main tasks:

 + install mongo
 + create config files

### hive-analytics.yml

Used to setup analytics server with Postgresql database and NodeJS process that will read
Redis queue. Will create empty DB from `db_dump.sql` file. You may have problems with locales,
if so, install locales with `locale-gen` package.

** IMPORTANT **

When you create config files for analytics cluster do not forget to specify the same values for
redis port/host, queue prefix/name properties. In other case cluster will not work properly.


### hive-analytics-collector.yml

Used to setup analytics collector server that will provide REST APIs for collecting analytics.
It will push all the requests into Redis queue.

### Log collector

** INFO **

Just as result of test you can find here `log-collector.yml`.
It has two types of tasks:

 + Graylog2 setup
 + rsyslog setup

Graylog2 uses Java, so [memory requirement is terrible](https://github.com/Graylog2/graylog2-server/issues/151#issuecomment-19154279).
rsyslog doesn't solve all required issues but can be easily configured on any Linux server.

So, these log-collecor.yml playbook and all its roles are leaved here for information. Maybe it will help somebody.