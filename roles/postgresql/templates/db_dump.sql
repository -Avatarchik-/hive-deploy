--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.6
-- Dumped by pg_dump version 9.3.1
-- Started on 2014-04-09 13:06:10 MSK

SET statement_timeout = 0;
-- SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2123 (class 1262 OID 49192)
-- Name: HiveAnalyticsTest; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "HiveAnalyticsTest" WITH TEMPLATE = template0 ENCODING = 'UTF-8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE "HiveAnalyticsTest" OWNER TO postgres;

\connect "HiveAnalyticsTest"

SET statement_timeout = 0;
-- SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2124 (class 1262 OID 49192)
-- Dependencies: 2123
-- Name: HiveAnalyticsTest; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE "HiveAnalyticsTest" IS 'Test database for analytics';


--
-- TOC entry 163 (class 3079 OID 11907)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2127 (class 0 OID 0)
-- Dependencies: 163
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 162 (class 1259 OID 49198)
-- Name: SessionsOne; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE "SessionsOne" (
    id character varying(12),
    start time without time zone,
    "end" time without time zone,
    platform character varying(6)
);


ALTER TABLE public."SessionsOne" OWNER TO postgres;

--
-- TOC entry 2128 (class 0 OID 0)
-- Dependencies: 162
-- Name: TABLE "SessionsOne"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "SessionsOne" IS 'Sessions of the users';


--
-- TOC entry 2129 (class 0 OID 0)
-- Dependencies: 162
-- Name: COLUMN "SessionsOne".id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "SessionsOne".id IS 'Mongodb id of the user';


--
-- TOC entry 2130 (class 0 OID 0)
-- Dependencies: 162
-- Name: COLUMN "SessionsOne".start; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "SessionsOne".start IS 'The time of session start';


--
-- TOC entry 2131 (class 0 OID 0)
-- Dependencies: 162
-- Name: COLUMN "SessionsOne"."end"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "SessionsOne"."end" IS 'The time of end session';


--
-- TOC entry 2132 (class 0 OID 0)
-- Dependencies: 162
-- Name: COLUMN "SessionsOne".platform; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "SessionsOne".platform IS 'The name of the platform that was used for the session';


--
-- TOC entry 161 (class 1259 OID 49193)
-- Name: Users; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE "Users" (
    id character varying(24) NOT NULL,
    created timestamp,
    country character varying(2),
    channel character varying(2)
);


ALTER TABLE public."Users" OWNER TO postgres;

--
-- TOC entry 2133 (class 0 OID 0)
-- Dependencies: 161
-- Name: TABLE "Users"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE "Users" IS 'Unique registered users';


--
-- TOC entry 2134 (class 0 OID 0)
-- Dependencies: 161
-- Name: COLUMN "Users".id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Users".id IS 'Mongodb of the user';


--
-- TOC entry 2135 (class 0 OID 0)
-- Dependencies: 161
-- Name: COLUMN "Users".created; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Users".created IS 'Registration date';


--
-- TOC entry 2136 (class 0 OID 0)
-- Dependencies: 161
-- Name: COLUMN "Users".country; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Users".country IS 'Two chars country code';


--
-- TOC entry 2137 (class 0 OID 0)
-- Dependencies: 161
-- Name: COLUMN "Users".channel; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "Users".channel IS 'Where is the user from';


--
-- TOC entry 2016 (class 2606 OID 49197)
-- Name: mongodb_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT mongodb_id PRIMARY KEY (id);


--
-- TOC entry 2017 (class 2606 OID 49201)
-- Name: user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "SessionsOne"
    ADD CONSTRAINT user_id FOREIGN KEY (id) REFERENCES "Users"(id);


--
-- TOC entry 2126 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-04-09 13:06:10 MSK

--
-- PostgreSQL database dump complete
--

